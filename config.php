<?
$config=array(
  "name" => "Дерево каталога",
  "status" => "system",
  "windows" => array(
                      "create" => array("width" => 1000,"height" => 700),
                      "edit" => array("width" => 550,"height" => 400),
                       "list" => array("width" => 800,"height" => 500),
                    ),
  "right" => array("admin","#GRANTED"),
  "main_table" => "lt_catalog_chapters",
  "list" => array(
    "name" => array("isLink" => true),
    "parent" => array("disabledSort" => true),
    #"propertys_list" => array(),
    "url" => array(),
    "orders" => array(),
  ),
  "select" => array(
     "max_perpage" => 50,
     "default_orders" => array(
                           array("parent" => "ASC"),
                           array("orders" => "ASC"),
                           array("name" => "ASC"),
                         ),
     "default" => array(
        "parent" => array(
               "desc" => "Родительская категория",
               "type" => "select_from_tree_table",
               "table" => "lt_catalog_chapters",
               "key_field" => "id_lt_catalog_chapters",
               "parent_field" => "parent",
               "fields" => array("name"),
               "show_field" => "%1",
               "condition" => "",
               "use_empty" => true,
               "in_list" => true,
               "is_root" => true,
               "empty_desc" => "&#187; Не важно",
               "root_desc" => "&#187; Корневой раздел",
               "order" => array ("orders" => "ASC","name" => "ASC"),
             ),

     ),
  ),
  "downlink_tables" => array(
    "lt_catalog_items" => array(
                      "key_field" => "id_catalog_chapters",
                      "name" => "name",
                      "component" => "catalog_items"
                    ),
  ),
);

?>