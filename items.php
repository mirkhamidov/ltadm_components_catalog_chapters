<?
$items=array(
  "name" => array(
         "desc" => "Название каталога",
         "type" => "text",
         "maxlength" => "255",
         "size" => "70",
         "select_on_edit" => true,
         "js_validation" => array(
           "js_not_empty" => "Поле должно быть не пустым!",
         ),
       ),

/*
  "propertys_list" => array(
    "desc" => "Набор свойств для этого каталога",
    "type" => "select_simple",
    "values" => array(
      ''=>'Нет свойств',
      'material,cvet,dlina_igolok,nizhnij_diametr,podstavka'=>'Ели комнатные',
      'type,material,konstrutia'=>'Ели высотные уличные',
      'cvet,dlina,vneshnij_diametr,kolichestvo_v_korobke'=>'Мишура',
      'dlina,kolichestvo_lamp,kolichestvo_funkcij,tip_lamp,cvet_provoda,kolichestvo_v_korobke'=>'Электрогирлянды',
      'diametr,cvet,poverhnost,kolichestvo_v_korobke'=>'Шары',
      'material,diametr,cvet'=>'Украшения для уличных елок',
      'razmer'=>'Хвойные гирлянды',
    ),
    "default_value" => "",
    "select_on_edit" => true,
  ),
*/



  "parent" => array(
         "desc" => "Родительский каталог",
         "type" => "select_from_tree_table",
         "table" => "lt_catalog_chapters",
         "key_field" => "id_lt_catalog_chapters",
         "parent_field" => "parent",
         "fields" => array("name"),
         "show_field" => "%1",
         "condition" => "",
//         "use_empty" => true,
         "is_root" => true,
         "root_desc" => "Корневой раздел",
         "order" => array ("orders" => "ASC"),
         "changeByLevel" => array (
           "0-0" => array(
                     enabled => array("is_onindex","photo_for_childs"),
                     disabled => array("is_onindex_ico"),
           ),
           "1-" => array(
                     enabled => array("is_onindex_ico"),
                     disabled => array("is_onindex","photo_for_childs"),
           ),

         ),
       ),

/*
  "is_inmenu" => array(
         "desc" => "Выводить это раздел в главное меню каталога?",
         "type" => "radio",
         "values" => array ("yes" => "Да", "no" => "Нет"),
         "default_value" => "no",
       ),
*/


  "url" => array(
         "desc" => "Идентификатор страницы",
         "type" => "text",

         "full_desc" => "Часть адреса страницы, по которой она будет доступна, только латинские символы, цифры и спецсимволы!",

         "maxlength" => "255",
         "size" => "70",
         "select_on_edit" => true,
         "get_from_field" => array(
           "field" => "name",
           "translate" => "latin",
           "case" => "strtolower",
         ),
         "js_validation" => array(
           "js_match" => array (
             "pattern" => "^[A-Za-z0-9_\-:/~]+$",
             "flags" => "g",
             "error" => "Только латинские символы, цифры и спецсимволы!",
           ),
         ),
         "select_on_edit" => true,
       ),


  "orders" => array(
         "desc" => "Порядок вывода в списках",
         "type" => "order",
         "js_validation" => array(
           "js_not_empty" => "Поле должно быть не пустым!",
           "js_match" => array (
             "pattern" => "^[\d]+$",
             "flags" => "g",
             "error" => "Только цифры! От 0 до 99999!",
           ),
         ),
       ),

/*
  "is_onindex" => array(
         "desc" => "Показывать на главной странице?",
         "full_desc" => "Только для корневых разделов",
         "type" => "radio",
         "values" => array ("yes" => "Да", "no" => "Нет"),
         "default_value" => "no",
       ),

  "is_onindex_ico" => array(
         "desc" => "Показывать иконкой на блоке?",
         "full_desc" => "Только для разделов второго уровня",
         "type" => "radio",
         "values" => array ("yes" => "Да", "no" => "Нет"),
         "default_value" => "no",
       ),
*/


  "photo" => array
  (
    "type"                  => "processed_image",
    "file_is"               => "processed_image",
    "desc"                  => "Фотография для изображения раздела",
    "full_desc"             => "Только изображения форматов JPEG, GIF и PNG",
    "store_source_name"     => true,          // сохранять ли имя загруженного файла, которое тот имеет на компьютере пользователя?
    "store_dimensions"      => true,          // хранить ли в БД размеры картинки, или определять их из файла (false)
    "check_file"            => true,          // проверять ли при отображении картинки существование файла, или доверять информации из БД (false)
    "check_dimensions"      => true,          // проверять ли при отображении картинки её размеры, или доверять информации из БД (false)
    "lt_show_system"        => true,          // разрешает отображать lt_source и lt_preview
    "lt_delete_files"       => true,          // разрешает возможность удалять файлы картинок для данной записи
    "lt_allow_name_change"  => false,          // разрешает загружать файл под именем, указанным пользователем
    "lt_preview"            => "preview",        // индекс картинки, которая считается картинкой для предварительного просмотра
    "lt_allow_remote_files" => true,

    "images" => array
    (
      "preview" => array
      (
        "prefix"  => "_",
        "target_type" => 2,
        "transforms" => array
        (
          "resize" => array("w"=>100 ,"h"=>85,"backgroundColor" => array(0,0,0)),
          #"crop" => true,
        ),
        "quality" => 100,
      ),

      "normal" => array
      (
        "prefix"  => "_",
        "target_type" => 2,
        "transforms" => array
        (
          "resize" => array("w"=>200 ,"h"=>200,"backgroundColor" => array(0,0,0)),
          #"crop" => true,
        ),
        "quality" => 100,
      ),


/*
      "big" => array
      (
        "prefix" => "big_",
        "target_type" => 2,
        "transforms" => array
        (
          "resize" => array("w" => 290 , "h" => 224,"backgroundColor" => array(0,0,0)),
          "crop" => true,
        ),
        "quality" => 100,
      ),
*/
    ),
    "select_on_edit" => true,
  ),


/*
  "photo_for_childs" => array
  (
    "type"                  => "processed_image",
    "file_is"               => "processed_image",
    "desc"                  => "Фотография для размещения рядом со списком подкатегорий",
    "full_desc"             => "Только изображения форматов JPEG, GIF и PNG",
    "store_source_name"     => true,          // сохранять ли имя загруженного файла, которое тот имеет на компьютере пользователя?
    "store_dimensions"      => true,          // хранить ли в БД размеры картинки, или определять их из файла (false)
    "check_file"            => true,          // проверять ли при отображении картинки существование файла, или доверять информации из БД (false)
    "check_dimensions"      => true,          // проверять ли при отображении картинки её размеры, или доверять информации из БД (false)
    "lt_show_system"        => true,          // разрешает отображать lt_source и lt_preview
    "lt_delete_files"       => true,          // разрешает возможность удалять файлы картинок для данной записи
    "lt_allow_name_change"  => false,          // разрешает загружать файл под именем, указанным пользователем
    "lt_preview"            => "small",        // индекс картинки, которая считается картинкой для предварительного просмотра
    "lt_allow_remote_files" => true,

    "images" => array
    (
      "small" => array
      (
        "prefix"  => "_",
        "target_type" => 2,
        "transforms" => array
        (
          "resize" => array("w"=>81 ,"h"=>136),
          "crop" => true,
        ),
        "quality" => 100,
      ),

    ),
    "select_on_edit" => true,
  ),
*/


  "content" => array(
         "desc" => "Описание каталога",
         "type" => "editor",
         "width" => "700",
         "height" => "500",
         "select_on_edit" => true,
       ),




  "name_title" => array(
         "desc" => "Название страницы в заголовке",
         "full_desc" => "Для страницы с подробным содержанием",
         "type" => "text",
         "maxlength" => "255",
         "size" => "70",

         "get_from_field" => array(
           "field" => "name",
         ),
       ),
  "classification" => array(
         "desc" => "Классификация страницы",
         "full_desc" => "Для страницы с подробным содержанием",
         "type" => "text",
         "maxlength" => "255",
         "size" => "70",
       ),
  "descrip" => array(
         "desc" => "Описание страницы",
         "full_desc" => "Для страницы с подробным содержанием",
         "type" => "textarea",
         "width" => "70",
         "height" => "5",
//         "select_on_edit" => true,
       ),
  "keywords" => array(
         "desc" => "Ключевые слова (через запятую)",
         "full_desc" => "Для страницы с подробным содержанием",
         "type" => "textarea",
         "width" => "70",
         "height" => "5",
//         "select_on_edit" => true,
       ),


);
?>